package edu.uprm.cse.datastructures.cardealer;




import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.JsonError;
import edu.uprm.cse.datastructures.cardealer.model.NotFoundException;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/cars")
public class CarManager {

  private final CircularSortedDoublyLinkedList<Car> carList= CarList.getInstance();
  					

  @GET
  //@Path("/all")
  //@Produces(MediaType.APPLICATION_XML)  //For XML Use, Now using JSON
  @Produces(MediaType.APPLICATION_JSON)
  public Car[] getAllCars() {
    Car[] cars = new Car[carList.size()];
    for(int i=0;i<carList.size();i++) {
    	cars[i]=carList.get(i);
    }
    return cars;
  }

  @GET
  @Path("{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Car getCar(@PathParam("id") long id){
	Car car=null;
    for(int i=0;i<carList.size();i++){
    	if(carList.get(i).getCarId()==id){
    		car=carList.get(i);
    	}
    }
    if(car==null) {
    	//Created JsonError class(in model package) for this to work
      throw new NotFoundException(new JsonError("Error", "Car " + id + " not found"));
    }
    return car;
  }
  
  @POST
  @Path("/add")
  @Produces(MediaType.APPLICATION_JSON)
  public Response addCar(Car car){
    carList.add(car);
  //This return takes care of given ID's 
    return Response.status(201).build();  
  }
  
  @PUT
  @Path("{id}/update")
  @Produces(MediaType.APPLICATION_JSON)
  public Response updateCar(Car car){
	  for(int i=0;i<carList.size();i++) {
			if(carList.get(i).getCarId()==car.getCarId()) {
				carList.remove(carList.get(i));
				carList.add(car);
				return Response.status(Response.Status.OK).build();
			}
	  }
	  return Response.status(Response.Status.NOT_FOUND).build(); 
    } 

  @DELETE
  @Path("/{id}/delete")
  public Response deleteCar(@PathParam("id") long id){
	    for(int i=0;i<carList.size();i++){
	    	if(carList.get(i).getCarId()==id){
	    		carList.remove(carList.get(i));
	    		return Response.status(Response.Status.OK).build();  
	    	}
	    }
	    return Response.status(Response.Status.NOT_FOUND).build();     
   }
}