package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{

	
	int size;
	Comparator<E> comp;
	
	@SuppressWarnings("hiding")
	class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
		
		Node<E> nextNode;
		
		@SuppressWarnings("unchecked")
		CircularSortedDoublyLinkedListIterator(){
			this.nextNode=(Node<E>) header.getNext();
		}
		
		@Override
		public boolean hasNext() {
			return this.nextNode.getElement()!=null;
		}

		@Override
		public E next() {
			if(this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode=this.nextNode.getNext();
				return result;
			}
			throw new NoSuchElementException();
		}
		
	}
	
	
	@SuppressWarnings("hiding")
	class Node<E>{
		E element;
		Node<E> previous;
		Node<E> next;
		
		public Node() {
			this.element=null;
			this.next=null;
			this.previous=null;
		}
		
		public Node(E element,Node<E> next, Node<E> previous) {
			this.element=element;
			this.next=next;
			this.previous=previous;
		}
		
		
		public E getElement() {
			return element;
		}
		
		public void setElement(E element) {
			this.element=element;
		}
		
		public Node<E> getNext() {
			return next;
		}

		public void setNext(Node<E> next) {
			this.next = next;
		}

		public Node<E> getPrevious() {
			return previous;
		}

		public void setPrevious(Node<E> previous) {
			this.previous = previous;
		}
	
		
 	}
	
	Node<E> header;
	
	/*
		No need for a constructor with NO PARAMETER, 
		because there always is need for a comparator.
	
	*/
	
	public CircularSortedDoublyLinkedList(Comparator<E> comp) {
		this.size=0;
		this.header= new Node<E>(null,this.header,this.header); 
		this.comp=comp;
	}

	
	@Override
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedListIterator<E>();
	}

	@Override
	public boolean add(E obj) {
		Node<E> temp = new Node<E>(obj,null,null);
		if(this.isEmpty()) {
			this.header.setNext(temp);
			this.header.setPrevious(temp);
			temp.setNext(this.header);
			temp.setPrevious(this.header);
			this.size++;
			return true;
		}
		Node<E> var=this.header.getNext();
		Node<E> greater=var;
		while(var.getElement()!=null) {
			if(this.comp.compare(obj, var.getElement())<=0){			
				temp.setNext(var);
				temp.setPrevious(var.getPrevious());
				var.getPrevious().setNext(temp);
				var.setPrevious(temp);
				this.size++;
				return true;
			}
			if(this.comp.compare(obj, var.getElement())>0){
				greater=var;
			}
			var=var.getNext();
		}
		temp.setPrevious(greater);
		temp.setNext(greater.getNext());
		greater.getNext().setPrevious(temp);
		greater.setNext(temp);
		this.size++;
		return true;
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean remove(E obj) {
		Node<E> temp=this.header.getNext();
		while(temp.getElement()!=null) {
			if(temp.getElement().equals(obj)) {	
				Node<E> temp2=temp.getPrevious();
				temp2.setNext(temp.getNext());
				temp.getNext().setPrevious(temp2);
				temp.setNext(null);
				temp.setPrevious(null);
				temp.setElement(null);
				this.size--;
				return true;
			}
			temp=temp.getNext();
		}	
		return false;
	}

	@Override
	public boolean remove(int index) {
		if(index<0 || index>=this.size) {
			throw new IndexOutOfBoundsException();
		}
		return this.remove(this.findNode(index).getElement());
	}

	private Node<E> findNode(int index) {
		Node<E> temp=this.header.getNext();
		int i=0;
		while(temp.getElement()!=null) {
			if(i==index) {
				return temp;
			}
			temp=temp.getNext();
			i++;
		}
		return null;
	}

	@Override
	public int removeAll(E obj) {
		int i =0;
		while(this.remove(obj)) {
			i++;
		}
		return i;
	}

	@Override
	public E first() {
		return this.header.getNext().getElement();
	}

	@Override
	public E last() {
		return this.header.getPrevious().getElement();
	}

	@Override
	public E get(int index) {	
		if(index<0 || index>=this.size) {
			throw new IndexOutOfBoundsException();
		}
		Node<E> temp=this.header.getNext();
		int i=0;
		while(temp.getElement()!=null) {
			if(i==index) {
				return temp.getElement();
			}
			temp=temp.getNext();
			i++;
		}
		return null;
	}

	@Override
	public void clear() {
		int i=0;
		while(!this.isEmpty()) {
			this.remove(i);
		}
	}

	@Override
	public boolean contains(E e) {
		for(int i=0;i<this.size;i++) {
			if(this.get(i).equals(e)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		return this.size==0;
	}

	@Override
	public int firstIndex(E e) {
		Node<E> temp=this.header.getNext();
		int i=0;
		while(temp.getElement()!=null) {
			if(temp.getElement().equals(e)){
				return i;
			}
			temp=temp.getNext();
			i++;
		}
		return -1;
	}

	@Override
	public int lastIndex(E e) {
		Node<E> temp=this.header.getPrevious();
		int i=this.size-1;
		while(temp.getElement()!=null) {
			if(temp.getElement().equals(e)){
				return i;
			}
			temp=temp.getPrevious();
			i--;
		}
		return -1;
	}

}
